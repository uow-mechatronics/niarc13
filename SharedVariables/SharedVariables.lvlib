﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="12008004">
	<Property Name="NI.Lib.Icon" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">302022660</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Enable Motors" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!")!A!1!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="gripper left pos (mm)" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&lt;&amp;Q!!!")!A!1!!!!"!!5!!A!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="gripper right pos (mm)" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&lt;&amp;Q!!!")!A!1!!!!"!!5!!A!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Grippers Disabled" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!")!A!1!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="LIDAR Data" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"_?A!!!")!A!1!!!!&amp;!!V!#A!(4H6N:8*J9Q!?1%!!!@````]!!"&amp;.97&gt;O;82V:'5A&lt;X6U)#BN+1!.1!I!"U2F:X*F:8-!)%"!!!(`````!!)42'FS:7.U;7^O)'^V&gt;#!I=G&amp;E+1!+!&amp;!!!A!"!!-!!1!%!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Previous Waypoint" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$HYQ!!!")!A!1!!!!(!!&gt;!#A!"7!!(1!I!!6E!#U!+!!65;'6U91!=1&amp;!!!Q!!!!%!!AZ3&lt;W*P&gt;#"1&lt;X.J&gt;'FP&lt;A!!5U!7!!-24X*J:7ZU982F)&amp;2P)%ZF?(174X*J:7ZU982F)&amp;2P)&amp;.Q:7.J:GFF:""/&lt;S"3:7^S;76O&gt;'&amp;U;7ZH!""0=GFF&lt;H2B&gt;'FP&lt;C"N&lt;W2F!!!D1"9!!AB'&lt;X*X98*E=QF#97.L&gt;W&amp;S:(-!!!:.&lt;X2J&lt;WY!!!Q!5!!$!!-!"!!&amp;!!%!"A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Robot Position" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"241!!!")!A!1!!!!%!!&gt;!#A!"7!!(1!I!!6E!#U!+!!65;'6U91!-!&amp;!!!Q!!!!%!!A!"!!-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Waypoint List" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">1</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Waypoint.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../Types/Waypoint.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&lt;&amp;Q%!!")!A!1!!!!+!!&gt;!#A!"7!!(1!I!!6E!#U!+!!65;'6U91!=1&amp;!!!Q!!!!%!!AZ3&lt;W*P&gt;#"1&lt;X.J&gt;'FP&lt;A!!5U!7!!-24X*J:7ZU982F)&amp;2P)%ZF?(174X*J:7ZU982F)&amp;2P)&amp;.Q:7.J:GFF:""/&lt;S"3:7^S;76O&gt;'&amp;U;7ZH!""0=GFF&lt;H2B&gt;'FP&lt;C"N&lt;W2F!!!D1"9!!AB'&lt;X*X98*E=QF#97.L&gt;W&amp;S:(-!!!:.&lt;X2J&lt;WY!!!Z!)1F6=W5A4%F%16)!#U!+!!2;&lt;WZF!!!T!0%!!!!!!!!!!1R898FQ&lt;WFO&gt;#ZD&gt;'Q!(E"1!!5!!Q!%!!5!"A!(#&amp;&gt;B?8"P;7ZU!!!-!%!!!@````]!#!!"!!E!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
